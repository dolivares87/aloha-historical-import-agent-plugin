/* eslint-disable */
import pkg from './package.json';
/* eslint-enable */

import loadFacts from './lib/pipelines/loadFacts';
import dateRangeGlob from './lib/services/dateRangeGlob';
import checkRunImport from './lib/pipelines/checkRunImport';

const NAMESPACE = 'historical';
const { name, version } = pkg;
const pluginConf = {
  name,
  version,
  attach(options, cb) {
    this.registerPlugin([NAMESPACE, 'dateRangeGlob'], dateRangeGlob);
    this.registerPlugin([NAMESPACE, 'loadFacts'], loadFacts);
    this.registerPlugin([NAMESPACE, 'checkRunImport'], checkRunImport);

    return cb && cb();
  }
};

export default pluginConf;
