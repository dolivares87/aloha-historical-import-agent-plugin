export default function loadFacts(input, next) {
  const aloha = this.aloha;
  const fileCfg = input;
  const { dir: currentFileDir } = fileCfg;
  const fileFacts = aloha.createFacts(currentFileDir);

  fileFacts.check((err, cfg) => {
    if (err) {
      next(err);
    } else if (!cfg || !cfg.current_dob) {
      this.logger.info('unable to detect the current day of business in the aloha.ini file');

      next(null, this.$BREAKER);
    } else {
      this.aloha.fileFacts = fileFacts;

      fileCfg.CURRENT_DOB = cfg.current_dob;

      next(null, fileCfg);
    }
  });
}
