export default function checkRunImport(input, next) {
  const numberOfWeeksToImport = this.numberOfWeeksToImport;

  if (numberOfWeeksToImport > 0) {
    this.logger.info('Running import job');
    return next(null);
  }

  this.logger.info('Importer not set');
  return next(null, this.$BREAKER);
}
