import moment from 'moment';
import 'moment-range';
import glob from '../glob';

function createError(msg) {
  return new Error(msg);
}

function createRangeGlob(numberOfWeeksToImport) {
  const today = moment();
  const startFrom = moment().subtract(numberOfWeeksToImport, 'weeks');
  const range = moment.range(startFrom, today);

  const datesArray = range.toArray('days');

  const dateRangeString = datesArray
    .map(date => {
      return moment(date)
        .format('YYYYMMDD');
    })
    .join('|');

  return dateRangeString;
}

function cleanseFileExtension(files) {
  return files.map(file => file.split('.')[0]);
}

export default function dateRangeGlob(env, numberOfWeeksToImport, files) {
  const fileExtensionGlob = '.+(d|D)+(b|B)+(f|F)';
  let filesToUse = files;

  if (!Array.isArray(filesToUse)) {
    filesToUse = [filesToUse];
  }

  return initialized => {
    if (!env) {
      return initialized(createError('Invalid Setting: No aloha environment specified'));
    }

    if (!files) {
      return initialized(createError('Invalid Dependencies: No files specified'));
    }

    if (!Array.isArray(files)) {
      return initialized(
        createError('Invalid Dependencies: Files must be a string or array of strings')
      );
    }

    const dateRangeString = `*(${createRangeGlob(numberOfWeeksToImport)})`;

    return initialized(null, glob(`%s/${dateRangeString}/*(%s)${fileExtensionGlob}`,
      env.IBERDIR, cleanseFileExtension(filesToUse)));
  };
}
