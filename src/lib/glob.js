import util from 'util';

export default function Glob(template, base, files) {
  return Object.create({
    compile() {
      return util.format(template, base.split('\\').join('/'), files.join('|'));
    }
  });
}
