import test from 'ava';
import dateRangeGlob from '../src/lib/services/dateRangeGlob';

test('dateRangeGlob(env, numberOfWeeksToImport, files)', t => {
  const files = ['test.dbf', 'teset2.dbf'];
  const numberOfWeeksToImport = 32;
  const env = {
    IBERDIR: 'C:\\Aloha'
  };

  const initializerFn = dateRangeGlob(env, numberOfWeeksToImport, files);

  t.true(typeof initializerFn === 'function', 'Should return initializer function');

  const initializeFn = (err, glob) => {
    console.log('glob', glob.compile());
    t.pass();
  };

  initializerFn(initializeFn);
});
